import {from, fromEvent, interval, Observable, of, scan } from 'rxjs';
import {filter, map} from 'rxjs/operators';

export function sample1() {
    const stream$ = of(1,2,4,5,"test");

    const subscription = stream$.subscribe({
        next: v => console.log(v),
        error: e => console.error(e),
        complete: () => console.log("complete"),
    });

    subscription.unsubscribe();
}

export function sample2() {
    const stream$ = new Observable(observer => {
        observer.next(1);
        observer.next(2);
        observer.next("3423324");
        
        setTimeout(() => {
            observer.error("some error");
            //observer.next(new Error("test"));
            observer.complete();
        }, 1000);

        //observer.complete();
    });

    const subscription = stream$.subscribe({
        next: v => console.log(v),
        error: e => console.error(e),
        complete: () => console.warn("complete"),
    });

    //subscription.unsubscribe();
}


export function sample3(element) {
    const stream$ = fromEvent(element, "click")
        .pipe(
            scan((count, value) => ++count, 0) // суть та же, что и в [...].reduce((acc, value) => ++acc, 0)
        );

    const subscription = stream$.subscribe({
        next: v => console.log("first observer: " + v),
        error: e => console.error(e),
        complete: () => console.log("complete stream at sample3"),
    });

    setTimeout(() => {
        stream$.subscribe({
            next: v => console.log("second observer: " + v),
            error: e => console.error(e),
            complete: () => console.log("complete stream at sample3"),
        });
    }, 3000);
}

export function sample4() {
    const stream$ = interval(1000)
    .pipe(
        map(v => v * Math.floor(Math.random() * 100)),
        filter(v => v % 3 !== 0)
    );
    
    const subscriptions = [];
    subscriptions.push(stream$.subscribe({
        next: v => console.log("first observer: " + v),
        error: e => console.error(e),
        complete: () => console.log("complete stream at sample3"),
    }));

    setTimeout(() => {
        subscriptions.push(
            stream$.subscribe({
                next: v => console.log("second observer: " + v),
                error: e => console.error(e),
                complete: () => console.log("complete stream at sample3"),
            })
        );
    }, 3000);

    setTimeout(() => subscriptions.forEach(s => s.unsubscribe()), 10000);
}
