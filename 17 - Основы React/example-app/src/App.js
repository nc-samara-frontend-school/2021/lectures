import "./App.css";
import "antd/dist/antd.css";
import logo from "./logo.svg";
import login from "./login.svg";
import { Layout, Modal } from "antd";
import { Content, Footer, Header } from "antd/lib/layout/layout";
import { TodoCards } from "./components/TodoCards";
import { useState } from "react";
import { LoginForm } from "./components/LoginForm";

function App() {
  const [isModalVisible, setModalVisible] = useState(false);
  const handleCancel = () => {
    setModalVisible(false);
  }
  const handleOpen = () => {
    setModalVisible(true);
  }
  return (
    <>
      <Layout>
        <Header className="app-header">    
          <div className="app-header__left">         
            <img src={logo} className="app-header__logo" alt="logo" />
                      <span className="app-header__title"></span>      
          </div>
          Our first React app         
          <img src={login} className="app-header__login" alt="login" onClick={handleOpen} />       
        </Header>
        <Content className="app-content">    
          <TodoCards />  
        </Content>
        <Footer>Your awesome footer</Footer>
      </Layout>
      <Modal
        title="Login"
        onCancel={handleCancel}
        visible={isModalVisible} // state!
        footer={null}
      >   
        <LoginForm login={handleCancel} /> 
      </Modal>
    </>
  );
}
export default App;
