import { Card, Skeleton } from "antd";
const { Meta } = Card;

export const CardLoading = ({ loading }) => {
  return (
    <Card style={{ width: 300, marginTop: 16 }}>
      <Skeleton loading={loading} active>
        <Meta title="Card title" description="This is the description" />
      </Skeleton>
    </Card>
  );
};
