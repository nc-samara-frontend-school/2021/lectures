import { Card, Checkbox } from "antd";

export const TodoCard = ({ task }) => {
  return (
    <Card title={`Task ${task.id}`}>
      <p>{task.title}</p>
      <Checkbox defaultChecked={task.completed} disabled>
        Completed
      </Checkbox>
    </Card>
  );
};
