import { Col, Row } from "antd";
import { useEffect, useState } from "react";
import { getTodos } from "../api/api";
import { CardLoading } from "./CardLoading";
import { TodoCard } from "./TodoCard";

export const TodoCards = (props) => {
  const [todos, setTodos] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getTodos().then((response) => {
      setLoading(false);
      setTodos(response);
    });
  }, []);

  const renderCards = () => {
    return todos.map((task) => {
      return (
        <Col xs={24} md={6} key={task.id}>
          <TodoCard task={task}></TodoCard>
        </Col>
      );
    });
  };

  return (
    <Row gutter={[16, 16]}>
      {loading ? <CardLoading loading={loading} /> : renderCards()}
    </Row>
  );
};
