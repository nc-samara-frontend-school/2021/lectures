const express = require("express");

class UserController {
    _router = new express.Router();
    apiURL;
    _db;

    constructor(apiURL, db) {
        this.apiURL = apiURL;
        this._db = db;

        this.registerGETUsers();
        this.registerPOSTCreateUsers();
    }

    get router() {
        return this._router;
    }

    registerGETUsers() {
        this._router.get(this.apiURL + "/users", (request, response) => {
            this._db.collection("users")
                .find()
                .toArray((err, result) => {
                    if (err) {
                        console.error(err);
                        response.status(500)
                            .text(err.message);
                        return;
                    }

                    response.send(JSON.stringify(result));
                });
        });
    }

    registerPOSTCreateUsers() {
        this._router.post(this.apiURL + "/users", (request, response) => {
            this._db.collection('users')
                .insertOne(request.body)
                .then(result => {
                    console.log('Saved to database');
                    response.send(result.ops);
                })
                .catch(err => {
                    console.error(err.message);
                    response.status(500).send(err);
                })
        });
    }
}

module.exports = UserController;
