function addRandomUser() {
    const user = {name: "Agent Smith " + Math.random()};
    let request = {
        method: "POST",
        body: JSON.stringify(user),
        headers: {
            'Accept': 'application/json;charset=UTF-8',
            'Content-Type': 'application/json;charset=UTF-8'
        }
    };
    fetch("/api/users", request)
        .then(response => response.json())
        .then(data => {
            console.log(data);
        });
}

function loadUsers() {
    fetch("/api/users")
        .then(response => response.json())
        .then(data => {
            console.log(data);
            const table = document.getElementById("userTable");
            table.innerHTML = "";
            data.forEach(user => {
                table.innerHTML += `<tr><td>${user._id}</td><td>${user.name}</td></tr>`;
            });
        });
}
