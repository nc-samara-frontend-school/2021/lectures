const express = require("express");
const UserController = require("./controllers/user.controller");
const MongoClient = require("mongodb").MongoClient;
const app = express();

app.use(express.json());
app.use(express.static("public"));

const uri = "mongodb+srv://dbUser:203RCJdLPa2K71BL@testcluster.pjuzm.mongodb.net/myFirstDatabase?retryWrites=true&w=majority";
const mongoClient = new MongoClient(uri, {
    ignoreUndefined: true,
});
mongoClient.connect(function (err, database) {
    if (err) {
        console.log("Error connection to mongodb");
        throw err;
    }
    const db = database.db("nc-test-db");

    app.use(new UserController("/api", db).router);

    app.listen(3000, function () {
        console.log("App listening on port 3000! [http://localhost:3000/]");
    });
});
