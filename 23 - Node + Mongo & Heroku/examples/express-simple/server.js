const express = require('express');
const app = express();

app.use(express.static('public'));

app.get('/api/hello', function (req, res) {
  res.send('Hello World!');
});

app.listen(3000, function () {
  console.log('App listening on port 3000! [http://localhost:3000/]');
});
