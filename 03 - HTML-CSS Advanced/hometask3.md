# Netcracker Frontend School

## Домашнее задание к лекции на тему "Продвинутый HTML\CSS"

### Задание

1. Ознакомиться с пунктами 1.11 – 1.16 из https://html5book.ru/osnovy-html/
2. Ознакомиться с пунктами 2.15 – 2.31 из https://html5book.ru/osnovy-css/
3. Изменить вёрстку сайта из прошлого задания, используя возможности HTML5 и CSS3. Предусмотреть:
+ Общую структуру (к примеру - хедер, футер, боковые поля, основной контент и т.д.) выстроить с использованием grid.
+ Блок с использованием flex-box.
+ Использовать теги HTML5.
+ Использовать переменные CSS3 Оценить удобство новых технологий =)
+ Сделать запись в changelog.md
5. Завести мердж реквест на мастер и отправить на ревью куратору

### Список источников

1.	[Справочники по CSS](https://html5book.ru/css-css3/) 
2.	[Справочник по HTML/CSS (акцент на HTML4 и CSS2)](http://htmlbook.ru/) 
3.	[w3schools](https://www.w3schools.com/ )
4.	[Grid layoutit](https://grid.layoutit.com/ )
5.	[BEM](https://ru.bem.info/methodology/quick-start/)

