# Netcracker Frontend School

## Домашнее задание к лекции на тему "TypeScript"
### Общие требования
* Код необходимо разместить в ветке Task_11.
* Сделать запись в changelog.md.
* После выполнения задания завести MR и отправить куратору.

### Задание
 Переработать задание из домашней работы номер 5 ("OOP in Javascript") таким образом, чтобы код был написан на TypeScript.
 * Использовать классы, интерфейсы, объединения типов.
 * Все функции и переменные должны быть полностью типизированы.
 * С использованием generics написать фабрику для создания объектов.
 * Для фабрики написать декоратор, который при создании объекта 
будет выводить в консоль строку _"Object **ObjectType** created!"_
 где **ObjectType** - тип создаваемого объекта.
 * Написать тесты для проверки работоспособности.
 * (*) Настроить eslint и его плагины для работы с TypeScript.
 #### (**)Продвинутое доп. задание:
 * Самостоятельно подробнее изучить работу декораторов. 
 * Написать декоратор класса `Injectable({key: string})`, который создает экземпляр класса и помещает его в список (Map) инстансов по переданному ключу. 
Список инстансов можно передавать в декоратор или использовать внешнюю глобальную переменную.
 * Написать декоратор поля `Inject(key)`, который ищет по ключу в списке экземпляр класса и присваивает его полю. 

Пример использования: 
```
@Injectable({key: "TestInjectable"})
class TestInjectable {}

class TestInject {
 @Inject("TestInjectable")
 public testedField: TestInjectable;

 public print(): void {
  console.log(this.testedField);
 }
}
```
 
### Самостоятельно изучить работу
1. Изучить [TS Handbook](https://www.typescriptlang.org/docs/handbook/intro.html)
2. Изучить [Utility Types](https://www.typescriptlang.org/docs/handbook/utility-types.html)

### Список источников
1. [Официальная документация](https://www.typescriptlang.org/) 
2. [Tutorial на русском языке](https://metanit.com/web/typescript/) 
3. [Подробнее про декораторы](https://habr.com/ru/company/ivi/blog/275003/) 
