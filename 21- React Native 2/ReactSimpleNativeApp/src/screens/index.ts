import {DetailsScreen} from './DetailsScreen';
import {HomeScreen} from './HomeScreen';
import {SettingsScreen} from "./SettingsScreen";

export {
    DetailsScreen,
    HomeScreen,
    SettingsScreen,
}
