import React from 'react';
import {View, Text, Button} from 'react-native';

export function HomeScreen({navigation}) {
    const {navigate} = navigation
    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <Text>Home Screen</Text>
            <Button
                title="Go to Details"
                onPress={() => navigate('Details', {
                        description: 'Frontend School',
                        imageUri: 'https://reactnative.dev/img/tiny_logo.png',
                        title: 'Details Screen'
                    })
                }
            />
        </View>
    )
}
