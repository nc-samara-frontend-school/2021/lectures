import React from 'react';
import {View, Text, Button, Image, NativeModules} from 'react-native';
const { ToastModule } = NativeModules;

export function DetailsScreen({route, navigation}) {
    const {goBack} = navigation;
    const {description, imageUri} = route.params;
    return (
        <View
            style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
        >
            <Text>{description}</Text>
            <Image
                style={{width: 100, height: 100, marginVertical: 50}}
                source={{uri: imageUri}}
            />
            <Button onPress={goBack} title="Go back"/>
            <Button
                onPress={() => {
                    ToastModule.showToast('Native Module Example')
                }}
                title="Show Toast"
            />
        </View>
    );
}
